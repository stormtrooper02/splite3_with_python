import sqlite3

'''
CREATE TABLE tbl_wohnungen(id INTEGER PRIMARY KEY, pps INTEGER, size INTEGER, price INTEGER, ZIP INTEGER, description VARCHAR, rooms INTEGER, publishername VARCHAR, link VARCHAR, willhaben_ID VARCHAR, querydate VARCHAR);
CREATE TABLE count_advertisements(id INTEGER PRIMARY KEY, date TEXT, weekday TEXT, time TEXT, count INTEGER);
CREATE TABLE count_publishers(publishername TEXT PRIMARY KEY, number_of_ads INTEGER);
'''
database = 'wohnungen.db'
def send_immo_to_DB(ImmoObject):
    conn = sqlite3.connect(database)

    # Get a cursor object
    c = conn.cursor()

    sql_query = "INSERT INTO tbl_wohnungen VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
    werte = (None, ImmoObject.pps, ImmoObject.size, ImmoObject.price, ImmoObject.zip,ImmoObject.description, ImmoObject.rooms, ImmoObject.publishername, ImmoObject.link, ImmoObject.name, ImmoObject.querydate)
    c.execute(sql_query, werte)
    conn.commit()
    conn.close()

def query_last_pps():
    conn = sqlite3.connect(database)
    c = conn.cursor()

    c.execute("SELECT pps from tbl_wohnungen ORDER BY id DESC LIMIT 25")
    try:
        LASTpps = []
        for element in c.fetchall():
            LASTpps.append(element[0])
    except:
        LASTpps = [1]
    # print(LASTpps, type(LASTpps))

    c.execute("SELECT link from tbl_wohnungen ORDER BY id DESC LIMIT 25")
    try:
        LASTlink = []
        for element in c.fetchall():
            LASTlink.append(element[0])
    except:
        LASTlink = ["xy"]
    # print(LASTlink, type(LASTlink))
    conn.close()

    return LASTpps, LASTlink

def send_immo_count(date, weekday, time, count):
    conn = sqlite3.connect(database)

    # Get a cursor object
    c = conn.cursor()

    sql_query = "INSERT INTO count_advertisements VALUES (?, ?, ?, ?, ?)"
    werte = ( None, date, weekday, time, count)
    c.execute(sql_query, werte)
    conn.commit()
    conn.close()
    return ()

def assign_advertisement_to_publisher(publishername):
    conn = sqlite3.connect(database)
    c = conn.cursor()
    c.execute("SELECT number_of_ads FROM count_publishers WHERE publishername = ?", publishername)
    print("assign")
    print(c.fetchall())
    conn.close()



def query_publishercount(publishername):
    return()

if __name__== "__main__":
    assign_advertisement_to_publisher('abc')